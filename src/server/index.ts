import express, { Express, Request, Response } from "express";

// security
import cors from 'cors'
import helmet from 'helmet'

// todo https

// root roouter
import rootRuter from '../routes'

// Create express app
const server: Express = express()

// define SERVER to use "/api" and use rootRouter from 'index.ts' in routes
// from this point onover: http://localhost:8000/api/...
server.use(
    '/api',
    rootRuter
    )

// static server
server.use(express.static('public'))

// todo mongoose connection

// securtiy config
server.use(helmet())
server.use(cors())

//content type config
server.use(express.urlencoded({ extended: true, limit: '50mb'}))
server.use(express.json({limit: '50mb'}))

// redirection config
// http://localhost:8000/ --> http://localhost:8000/api/
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api')
})

export default server