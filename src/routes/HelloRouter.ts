import { BasicResponse } from "@/controller/types";
import express, { Request, Response } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";

// Router from express
let helloRouter = express.Router()

// http://localhost:8000/api/hello?name=Pino/
helloRouter.route('/')
    // GET -> http://localhost:8000/api/hello?name=Pino/
    .get(async (req: Request, res: Response) => {
        // obtain a query param
        let name: any = req?.query?.name
        LogInfo(`Query param: ${name}`)
        // controller instance to execute method
        const controller: HelloController = new HelloController()
        // obtain response
        const response: BasicResponse = await controller.getMessage(name)
        // sent to the client the response
        return res.send(response)
    })

// exprt hello router
export default helloRouter