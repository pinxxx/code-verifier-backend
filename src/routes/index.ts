/**
 * Root router
 * redirections to Router
 */

import express, { Request, Response } from "express";
import helloRouter from "./HelloRouter";
import { LogInfo } from "../utils/logger";

// server instance
let server = express()

//Router instance
let rootRouter = express.Router()

// Activate for request to http://localhost:8000/api

//GET: http://localhost:8000/api
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api')
    // send hello world
    res.send('Welcome to APP Restfull: Express + TS + Nodemon + Jest + Swagger + Mongoose')
})

// Redirections to routers & controllers
server.use('/', rootRouter) // http://localhost:8000/api
server.use('/hello', helloRouter) // http://localhost:8000/api/hello
// Add more routes to the app

export default server