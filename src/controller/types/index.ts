// Basic JSON repsonse for controllers
export type BasicResponse = {
    message: string
}

// eroor JSON repsonse for controllers
export type ErrorResponse = {
    error: string,
    message: string
}